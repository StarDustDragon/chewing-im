use rusttype::{point};
use wayland_client::{
    protocol::wl_shm::{self},
};

use crate::state::{State, HEIGHT, CANDIDATES, BACKGROUND_COLOR, A, SELECTION_COLOR, FONT_COLOR};

//TODO: Rounded corners, space for numbers, compact mode
fn create_glyphs(
    state: &State,
    list: &[String]
) -> (Vec<[u8; 4]>, usize, usize) {
    let candidates = if list.len() < CANDIDATES {
        list.len()
    } else {
        CANDIDATES
    };
    let suggestions = (list.len() as f32 / candidates as f32).ceil() as usize;
    let remainder = if list.len() % candidates == 0 {
        list.len()
    } else {
        list.len() % candidates
    };

    let mut width = 0.0;

    let mut widths: Vec<f32> = Vec::new();
    let mut square_widths: Vec<f32> = Vec::new();

    for x in 0..suggestions {
        let chars_len = list[x * candidates].chars().count() as f32 * state.char_width + state.padding;
        width += chars_len;
        square_widths.push(chars_len);
        widths.push(width)
    }
    width += state.num_width;

    let mut squares: Vec<[u8; 4]> = Vec::new();

    for y in 0..candidates {
        let mut bitmap_square = vec![
            [
                BACKGROUND_COLOR[0],
                BACKGROUND_COLOR[1],
                BACKGROUND_COLOR[2],
                A
            ];
            width.ceil() as usize * HEIGHT
        ];
        for x in 0..suggestions {
            if x == suggestions - 1 && y >= remainder {
                break;
            }
            let bg_color = if x == state.selected.0 && y == state.selected.1 {
                // Draw square
                let square_starting_point = (widths[x] - square_widths[x]).ceil() as usize;
                let square_ending_point = (widths[x] + state.num_width).ceil() as usize;

                for y in 0..HEIGHT {
                    for x in square_starting_point..square_ending_point {
                        bitmap_square[x + y * width.ceil() as usize] = [
                            SELECTION_COLOR[0],
                            SELECTION_COLOR[1],
                            SELECTION_COLOR[2],
                            A,
                        ];
                    }
                }
                SELECTION_COLOR
            } else {
                BACKGROUND_COLOR
            };
            let item = &list[y + x * candidates];
            let item_width = item.chars().count() as f32 * state.char_width;
            let mut diff = (square_widths[x] - item_width) / 2.0 + (widths[x] - square_widths[x]); //Centered

            if state.selected.0 <= x {
                diff += state.num_width;
            }
            let glyphs: Vec<_> = state.font
                .layout(item, state.scale, point(diff, state.v_metrics.ascent))
                .collect();
            for glyph in glyphs {
                let bb = glyph.pixel_bounding_box().unwrap();
                glyph.draw(|x, y, v| {
                    let color = aa_bgra(bg_color, FONT_COLOR, v, A);
                    bitmap_square[(x as u32 + bb.min.x as u32) as usize
                        + (y + bb.min.y as u32) as usize * width.ceil() as usize] = color;
                });
            }
        }
        let bg_color = if y == state.selected.1 {
            SELECTION_COLOR
        } else {
            BACKGROUND_COLOR
        };
        if state.selected.0 != suggestions - 1 || y < remainder {
            let num_glyph = state.font
                .glyph(((y + 1) % 10).to_string().chars().last().unwrap())
                .scaled(state.scale)
                .positioned(point(
                    widths[state.selected.0] - square_widths[state.selected.0] + state.num_width / 3.0,
                    state.v_metrics.ascent,
                ));
            let bb = num_glyph.pixel_bounding_box().unwrap();
            num_glyph.draw(|x, y, v| {
                bitmap_square[(x + bb.min.x as u32) as usize
                    + (y + bb.min.y as u32) as usize * width.ceil() as usize] =
                    aa_bgra(bg_color, FONT_COLOR, v, A);
            });
        }
        squares.append(&mut bitmap_square);
    }
    (squares, width.ceil() as usize, candidates * HEIGHT)
}

fn aa_bgra(bg_color: [u8; 3], font_color: [u8; 3], v: f32, a: u8) -> [u8; 4] {
    let a: u8 = a;
    let r: u8 = ((1.0 - v) * bg_color[2] as f32 + v * font_color[2] as f32) as u8;
    let g: u8 = ((1.0 - v) * bg_color[1] as f32 + v * font_color[1] as f32) as u8;
    let b: u8 = ((1.0 - v) * bg_color[0] as f32 + v * font_color[0] as f32) as u8;
    [b, g, r, a]
}

pub fn draw_buffer(state: &mut State) {
    let set = state.set;
    let list = &state.list[set.0..set.1];
    if list.is_empty() {
        return;
    };
    let (squares, width, height) = create_glyphs(&state, list);
    let stride = (width * 4) as i32;

    let pool = state.pool.as_mut().unwrap();
    
    let buffer = state.buffer.get_or_insert_with(|| {
        pool.create_buffer(width as i32, height as i32, stride, wl_shm::Format::Argb8888)
            .expect("create buffer")
            .0
    });

    let canvas = match pool.canvas(buffer) {
        Some(canvas) => canvas,
        None => {
            let (second_buffer, canvas) = pool
                .create_buffer(
                    width as i32,
                    height as i32,
                    stride,
                    wl_shm::Format::Argb8888,
                )
                .expect("create buffer");
            *buffer = second_buffer;
            canvas
        }
    };

    canvas.chunks_exact_mut(4).enumerate().for_each(|(index, chunk)| {
        let array: &mut [u8; 4] = chunk.try_into().unwrap();
        *array = squares[index];
    });

    let surface = state.surface.as_ref().unwrap();
    surface.damage(0, 0, width as i32, height as i32);
    buffer.attach_to(surface).expect("buffer attach");
    surface.commit();
}
