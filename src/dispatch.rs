use std::{time::Duration, os::unix::prelude::AsRawFd};

use smithay_client_toolkit::shm::{ShmState, slot::SlotPool};
use wayland_client::{
    protocol::{
        wl_buffer::{self, WlBuffer},
        wl_compositor::{self, WlCompositor},
        wl_registry::{self, WlRegistry},
        wl_seat::{self, WlSeat},
        wl_shm::{self, WlShm},
        wl_shm_pool::{self, WlShmPool},
        wl_surface::{self, WlSurface}, wl_keyboard,
    },
    Connection, Dispatch, QueueHandle,
};
use wayland_protocols_misc::{
    zwp_input_method_v2::client::{
        zwp_input_method_keyboard_grab_v2::{self, ZwpInputMethodKeyboardGrabV2},
        zwp_input_method_manager_v2::ZwpInputMethodManagerV2,
        zwp_input_method_v2::{self, ZwpInputMethodV2},
        zwp_input_popup_surface_v2::{self, ZwpInputPopupSurfaceV2},
    },
    zwp_virtual_keyboard_v1::client::{
        zwp_virtual_keyboard_manager_v1::ZwpVirtualKeyboardManagerV1,
        zwp_virtual_keyboard_v1::ZwpVirtualKeyboardV1,
    },
};

use crate::{
    keys::{self, get_keymap_as_file},
    state::{Key, Modifiers},
};

use super::state::State;

impl Dispatch<WlRegistry, ()> for State {
    fn event(
        state: &mut Self,
        registry: &WlRegistry,
        event: wl_registry::Event,
        _: &(),
        _: &Connection,
        qh: &QueueHandle<Self>,
    ) {
        if let wl_registry::Event::Global {
            name, interface, ..
        } = event
        {
            match &interface[..] {
                "wl_compositor" => {
                    let compositor = registry.bind::<WlCompositor, _, _>(name, 1, qh, ());
                    let surface = compositor.create_surface(qh, ());
                    state.surface = Some(surface);
                }
                "wl_shm" => {
                    let shm = registry.bind::<WlShm, _, _>(name, 1, qh, ());
                    let shm_state = ShmState::from(shm);
                    let pool=SlotPool::new(1,&shm_state).expect("Failed to create pool");
                    state.pool = Some(pool);
                }
                "wl_seat" => {
                    registry.bind::<WlSeat, _, _>(name, 1, qh, ());
                }
                "zwp_input_method_manager_v2" => {
                    let input_method_manager =
                        registry.bind::<ZwpInputMethodManagerV2, _, _>(name, 1, qh, ());
                    state.input_method_manager = Some(input_method_manager);
                }
                "zwp_virtual_keyboard_manager_v1" => {
                    let virtual_keyboard_manager =
                        registry.bind::<ZwpVirtualKeyboardManagerV1, _, _>(name, 1, qh, ());
                    state.virtual_keyboard_manager = Some(virtual_keyboard_manager);
                }
                _ => {}
            }
        }
    }
}

impl Dispatch<ZwpInputMethodManagerV2, ()> for State {
    fn event(
        _state: &mut Self,
        _input_method_manager: &ZwpInputMethodManagerV2,
        _event: <ZwpInputMethodManagerV2 as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &Connection,
        _qh: &QueueHandle<Self>,
    ) {
        // Ignore zwp_input_manager events
    }
}

impl Dispatch<ZwpVirtualKeyboardManagerV1, ()> for State {
    fn event(
        _state: &mut Self,
        _proxy: &ZwpVirtualKeyboardManagerV1,
        _event: <ZwpVirtualKeyboardManagerV1 as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &Connection,
        _qhandle: &QueueHandle<Self>,
    ) {
        // Ignore zwp_virtual_keyboard_manager events
    }
}

impl Dispatch<WlCompositor, ()> for State {
    fn event(
        _: &mut Self,
        _: &wl_compositor::WlCompositor,
        _: wl_compositor::Event,
        _: &(),
        _: &Connection,
        _: &QueueHandle<Self>,
    ) {
        // wl_compositor has no event
    }
}

impl Dispatch<WlSurface, ()> for State {
    fn event(
        _: &mut Self,
        _: &wl_surface::WlSurface,
        _: wl_surface::Event,
        _: &(),
        _: &Connection,
        _: &QueueHandle<Self>,
    ) {
        // ignore wl_surface events
    }
}

impl Dispatch<WlShm, ()> for State {
    fn event(
        _: &mut Self,
        _: &wl_shm::WlShm,
        _: wl_shm::Event,
        _: &(),
        _: &Connection,
        _: &QueueHandle<Self>,
    ) {
        // ignore wl_shm events
    }
}

impl Dispatch<WlShmPool, ()> for State {
    fn event(
        _: &mut Self,
        _: &wl_shm_pool::WlShmPool,
        _: wl_shm_pool::Event,
        _: &(),
        _: &Connection,
        _: &QueueHandle<Self>,
    ) {
        // ignore wl_shm_pool events
    }
}

impl Dispatch<WlBuffer, ()> for State {
    fn event(
        _: &mut Self,
        _: &wl_buffer::WlBuffer,
        _: wl_buffer::Event,
        _: &(),
        _: &Connection,
        _: &QueueHandle<Self>,
    ) {
        // ignore wl_buffer events
    }
}

impl Dispatch<WlSeat, ()> for State {
    fn event(
        state: &mut Self,
        seat: &wl_seat::WlSeat,
        _event: wl_seat::Event,
        _: &(),
        _: &Connection,
        qh: &QueueHandle<Self>,
    ) {
        // TODO: pointer handling
        if let Some(input_method_manager) = state.input_method_manager.as_ref() {
            let input_method = input_method_manager.get_input_method(seat, qh, ());
            input_method.grab_keyboard(qh, ());
            state.input_method = Some(input_method);
        }
        if let Some(virtual_keyboard_manager) = state.virtual_keyboard_manager.as_ref() {
            let virtual_keyboard = virtual_keyboard_manager.create_virtual_keyboard(seat, qh, ());
            let (file, size) = get_keymap_as_file(state);
            virtual_keyboard.keymap(wl_keyboard::KeymapFormat::XkbV1.into(), file.as_raw_fd(), size);
            state.virtual_keyboard = Some(virtual_keyboard);
        }
    }
}

impl Dispatch<ZwpVirtualKeyboardV1, ()> for State {
    fn event(
        _state: &mut Self,
        _proxy: &ZwpVirtualKeyboardV1,
        _event: <ZwpVirtualKeyboardV1 as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &Connection,
        _qhandle: &QueueHandle<Self>,
    ) {
        // virtual keyboard has no events
    }
}

impl Dispatch<ZwpInputMethodKeyboardGrabV2, ()> for State {
    fn event(
        main_state: &mut Self,
        _proxy: &ZwpInputMethodKeyboardGrabV2,
        event: <ZwpInputMethodKeyboardGrabV2 as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &Connection,
        qh: &QueueHandle<Self>,
    ) {
        match event {
            zwp_input_method_keyboard_grab_v2::Event::Keymap {
                format: _,
                fd: _,
                size: _,
            } => {
                // Ignore since we are specifying our own keymap
            }
            zwp_input_method_keyboard_grab_v2::Event::Key {
                serial,
                time,
                key,
                state,
            } => {
                main_state.key_event = Key {
                    serial,
                    time,
                    key,
                    state: state.into_result().unwrap(),
                };
                keys::key(main_state, qh);
            }
            zwp_input_method_keyboard_grab_v2::Event::Modifiers {
                serial,
                mods_depressed,
                mods_latched,
                mods_locked,
                group,
            } => {
                main_state.modifiers = Modifiers {
                    serial,
                    mods_depressed,
                    mods_latched,
                    mods_locked,
                    group,
                }
            }
            zwp_input_method_keyboard_grab_v2::Event::RepeatInfo { rate, delay } => {
                main_state.rate = Duration::from_millis(1000 / rate as u64);
                main_state.delay = Duration::from_millis(delay as u64);
            }
            _ => unreachable!(),
        }
    }
}

impl Dispatch<ZwpInputMethodV2, ()> for State {
    fn event(
        state: &mut Self,
        _proxy: &ZwpInputMethodV2,
        event: <ZwpInputMethodV2 as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &Connection,
        _qhandle: &QueueHandle<Self>,
    ) {
        match event {
            zwp_input_method_v2::Event::Activate => {
                state.active = true;
                println!("Activate")
            }
            zwp_input_method_v2::Event::Deactivate => {
                state.active = false;
                if let Some(popup) = &state.popup_surface {
                    popup.destroy();
                }
                state.popup_surface = None;
                println!("deActivate")
            }
            zwp_input_method_v2::Event::SurroundingText {
                text: _,
                cursor: _,
                anchor: _,
            } => {}
            zwp_input_method_v2::Event::TextChangeCause { cause: _ } => {}
            zwp_input_method_v2::Event::ContentType {
                hint: _,
                purpose: _,
            } => {}
            zwp_input_method_v2::Event::Done => println!("done??"),
            zwp_input_method_v2::Event::Unavailable => {
                panic!("Another input method already present!")
            }
            _ => unreachable!(),
        }
    }
}

impl Dispatch<ZwpInputPopupSurfaceV2, ()> for State {
    fn event(
        state: &mut Self,
        _proxy: &ZwpInputPopupSurfaceV2,
        event: <ZwpInputPopupSurfaceV2 as wayland_client::Proxy>::Event,
        _data: &(),
        _conn: &Connection,
        _qhandle: &QueueHandle<Self>,
    ) {
        match event {
            zwp_input_popup_surface_v2::Event::TextInputRectangle {
                x,
                y,
                width: _,
                height: _,
            } => {
                state.x = x;
                state.y = y;
            }
            _ => unreachable!(),
        }
    }
}
