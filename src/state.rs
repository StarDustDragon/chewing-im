use std::{fs::File, time::Duration};

use chewing_rs::Chewing;
use rusttype::{Font, Scale};
use smithay_client_toolkit::{
    shm::{slot::{SlotPool, Buffer}},
};
use wayland_client::{
    protocol::{wl_keyboard::KeyState, wl_shm::WlShm, wl_surface::WlSurface}
};
use wayland_protocols_misc::{
    zwp_input_method_v2::client::{
        zwp_input_method_manager_v2::ZwpInputMethodManagerV2,
        zwp_input_method_v2::ZwpInputMethodV2, zwp_input_popup_surface_v2::ZwpInputPopupSurfaceV2,
    },
    zwp_virtual_keyboard_v1::client::{
        zwp_virtual_keyboard_manager_v1::ZwpVirtualKeyboardManagerV1,
        zwp_virtual_keyboard_v1::ZwpVirtualKeyboardV1,
    },
};
use xkbcommon::xkb;

pub const FONT_DATA: &[u8] = include_bytes!("../WenQuanYiZenHeiSharpRegular.ttf");

pub const CANDIDATES: usize = 10; // <= 10
pub const HEIGHT: usize = 40;
pub const A: u8 = 255;
pub const FONT_COLOR: [u8; 3] = [0, 0, 0]; //bgra
pub const SELECTION_COLOR: [u8; 3] = [237, 149, 100]; //bgra
pub const BACKGROUND_COLOR: [u8; 3] = [255, 255, 255]; //bgra

pub struct Modifiers {
    pub serial: u32,
    pub mods_depressed: u32,
    pub mods_latched: u32,
    pub mods_locked: u32,
    pub group: u32,
}

pub struct Key {
    pub serial: u32,
    pub time: u32,
    pub key: u32,
    pub state: KeyState,
}

pub struct State {
    pub input_method_manager: Option<ZwpInputMethodManagerV2>,
    pub input_method: Option<ZwpInputMethodV2>,
    pub virtual_keyboard_manager: Option<ZwpVirtualKeyboardManagerV1>,
    pub virtual_keyboard: Option<ZwpVirtualKeyboardV1>,
    pub active: bool,
    pub surface: Option<WlSurface>,
    pub popup_surface: Option<ZwpInputPopupSurfaceV2>,
    pub file: File,
    pub font: Font<'static>,
    pub x: i32,
    pub y: i32,
    pub chewing: Chewing,
    pub modifiers: Modifiers,
    pub shm: Option<WlShm>,
    pub list: Vec<String>,
    pub selected: (usize, usize),
    pub list_expanded: bool,
    pub xkb_state: xkb::State,
    pub rate: Duration,
    pub delay: Duration,
    pub pressing: bool,
    pub key_event: Key,
    pub set: (usize, usize),
    pub pool: Option<SlotPool>,
    pub buffer: Option<Buffer>,
    pub scale: Scale,
    pub v_metrics: rusttype::VMetrics,
    pub char_width: f32,
    pub num_width: f32,
    pub padding: f32,
}

impl State {
    pub fn init() -> State {
        let font = rusttype::Font::try_from_bytes(FONT_DATA).unwrap();
        let scale = rusttype::Scale::uniform(HEIGHT as f32);
        let char_width = font.glyph('灣').scaled(scale).h_metrics().advance_width;
        let context = xkb::Context::new(xkb::CONTEXT_NO_FLAGS);
        let keymap = xkb::Keymap::new_from_names(
            &context,
            "",
            "",
            "no",
            "",
            None,
            xkb::KEYMAP_COMPILE_NO_FLAGS,
        )
        .expect("xkbcommon keymap panicked!");
        State {
            v_metrics: font.v_metrics(scale),
            char_width,
            num_width: font.glyph('0').scaled(scale).h_metrics().advance_width * 1.5,
            padding: char_width / 2.0,
            scale,
            input_method_manager: None,
            input_method: None,
            virtual_keyboard_manager: None,
            virtual_keyboard: None,
            active: false,
            surface: None,
            buffer: None,
            popup_surface: None,
            file: tempfile::tempfile().unwrap(),
            font,
            x: 0,
            y: 0,
            chewing: Chewing::new().expect("libchewing not found"),
            modifiers: Modifiers {
                mods_depressed: 0,
                serial: 0,
                mods_latched: 0,
                mods_locked: 0,
                group: 0,
            },
            shm: None,
            list: Vec::new(),
            selected: (0, 0),
            list_expanded: false,
            xkb_state: xkb::State::new(&keymap),
            rate: Duration::from_millis(1000 / 25),
            delay: Duration::from_millis(250),
            pressing: false,
            key_event: Key {
                serial: 0,
                time: 0,
                key: 0,
                state: KeyState::Released,
            },
            set: (0, 0),
            pool: None,
        }
    }
}
